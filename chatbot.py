import os
import json
import boto3
from time import sleep
from boto3.dynamodb.conditions import Key, Attr
import requests

# bot setup: greeting and start button
def setup():
    params = {
        "access_token": os.environ["PAGE_ACCESS_TOKEN"]
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({ 
        "get_started":{
            "payload":"start"
        },
        "greeting":[
        {
            "locale":"default",
            "text":"Český rozhlas pro vás v rámci tematického týdne Tahle země bude pro starý připravil facebookovou hru, ve které si zkusíte vyžít s průměrným důchodem. Začněte!"
        }
        ]
    })    
    r = requests.post("https://graph.facebook.com/v2.6/me/messenger_profile", params=params, headers=headers, data=data)

def webhook(event, context):
    data = event["cont"]
    
    # endpoint for processing incoming messaging events

    if data["object"] == "page":
        messaging_event = data["entry"][0]["messaging"][0]
        if messaging_event.get("message"):  # someone sent us a message
            sender_id = messaging_event["sender"]["id"]        # the facebook ID of the person sending you the message
            message_text = messaging_event["message"]["text"]  # the message's text
            process_message(sender_id, message_text)

        if messaging_event.get("postback"):  # user clicked/tapped "postback" button in earlier message
            sender_id = messaging_event["sender"]["id"]        # the facebook ID of the person sending you the message
            command = messaging_event["postback"]["payload"]   # the command directing the game
            game(sender_id, command)

# decision tree
def game(sender_id, command):
    #### decision tree
    # bot welcome message
    if command == "start":
        msg_text = 'Vítejte ve facebookové hře, kterou pro vás připravil Český rozhlas! Během několika minut zjistíte, jestli byste dokázali vyžít celý měsíc se starobním důchodem.'        
        msg_choice = {"Jdeme na to!": "startgame"}
    # starting the game
    elif command == "startgame":
        msg_text = 'Výborně! Můžete hrát za muže z Karviné s důchodem 14 089 Kč, za ženu z Prahy s důchodem 11 399 Kč, nebo za ženu z Karviné s důchodem 9 599 Kč. Hru můžete kdykoliv spustit od začátku zprávou "restart".'
        msg_choice = {"Muž, Karviná": "dilemma_1_easy", "Žena, Praha": "dilemma_1_medium", "Žena, Karviná": "dilemma_1_hard"}
    # setting difficulty + first dilemma
    elif command == "dilemma_1_easy":
        msg_pre = "Pojďme na to. Na začátku měsíce tedy hospodaříte s částkou " + str(init_score(sender_id, 14089, "easy")) + " Kč. Čekají vás různé pravidelné i mimořádné výdaje, takže uvidíme, jestli vám tyto finance postačí."
        msg_text = "Rovnou odečtěte náklady na bydlení, vodu, energie a paliva, což je 6000 Kč. Zbývá vám " + str(set_score(sender_id, -6000)) + " Kč. Blíží se ovšem narozeniny vašeho vnuka, který si ze všeho nejvíc přeje dron s kamerou. Koupíte mu ho za 2000 Kč, i když to výrazně zatíží váš napjatý rozpočet?"
        msg_choice = {"Dárek koupím": "dilemma_2_a", "Dárek nekoupím": "dilemma_2_b"}
        send_message(sender_id, msg_pre)
    elif command == "dilemma_1_medium":
        msg_pre = "Pojďme na to. Na začátku měsíce tedy hospodaříte s částkou " + str(init_score(sender_id, 11399, "medium")) + " Kč. Čekají vás různé pravidelné i mimořádné výdaje, takže uvidíme, jestli vám tyto finance postačí."
        msg_text = "Rovnou odečtěte náklady na bydlení, vodu, energie a paliva, což je 6000 Kč. Zbývá vám " + str(set_score(sender_id, -6000)) + " Kč. Blíží se ovšem narozeniny vašeho vnuka, který si ze všeho nejvíc přeje dron s kamerou. Koupíte mu ho za 2000 Kč, i když to výrazně zatíží váš napjatý rozpočet?"
        msg_choice = {"Dárek koupím": "dilemma_2_a", "Dárek nekoupím": "dilemma_2_b"}
        send_message(sender_id, msg_pre)
    elif command == "dilemma_1_hard":
        msg_pre = "Pojďme na to. Na začátku měsíce tedy hospodaříte s částkou " + str(init_score(sender_id, 9599, "hard")) + " Kč. Čekají vás různé pravidelné i mimořádné výdaje, takže uvidíme, jestli vám tyto finance postačí."
        msg_text = "Rovnou odečtěte náklady na bydlení, vodu, energie a paliva, což je 6000 Kč. Zbývá vám " + str(set_score(sender_id, -6000)) + " Kč. Blíží se ovšem narozeniny vašeho vnuka, který si ze všeho nejvíc přeje dron s kamerou. Koupíte mu ho za 2000 Kč, i když to výrazně zatíží váš napjatý rozpočet?"
        msg_choice = {"Dárek koupím": "dilemma_2_a", "Dárek nekoupím": "dilemma_2_b"}
        send_message(sender_id, msg_pre)              

    # second dilemma
    elif command == "dilemma_2_a":
        msg_pre = "Sice si nebudete moct dopřát úplně všechno jako obvykle, ale vnukova radost stojí za všechny peníze, ne?"
        if get_value(sender_id,"difficulty") == "easy": 
            food_a = "4000"
            food_b = "2000"
        elif get_value(sender_id,"difficulty") == "medium": 
            food_a = "3500"
            food_b = "1800"
        elif get_value(sender_id,"difficulty") == "hard":  
            food_a = "3000"
            food_b = "1500"
        msg_text = "Z důchodu vám zbývá " + str(set_score(sender_id, -2000)) + " Kč, ale musíte počítat s několika nákupy potravin. Buď si můžete dopřát své oblíbené pochoutky (v tom případě utratíte "+food_a+" Kč), nebo si něco odepřít a koupit jen to základní a nejlevnější (pak utratíte "+food_b+" Kč). Co uděláte?"
        msg_choice = {"Dopřeju si": "dilemma_22_a", "Uskromním se": "dilemma_22_b"}
        send_message(sender_id, msg_pre)
    elif command == "dilemma_2_b":
        msg_pre = "Váš vnuk sice neměl takovou radost, jakou byste si mu přáli udělat, ale tak to v životě chodí. I tak vás dárek stál 600 korun."
        if get_value(sender_id,"difficulty") == "easy": 
            food_a = "4000"
            food_b = "2000"
        elif get_value(sender_id,"difficulty") == "medium": 
            food_a = "3500"
            food_b = "1800"
        elif get_value(sender_id,"difficulty") == "hard":  
            food_a = "3000"
            food_b = "1500"
        msg_text = "Z důchodu vám zbývá " + str(set_score(sender_id, -600)) + " Kč, ale musíte počítat s několika nákupy potravin. Buď si můžete dopřát své oblíbené pochoutky (v tom případě utratíte "+food_a+" Kč), nebo si něco odepřít a koupit jen to základní a nejlevnější (pak utratíte "+food_b+" Kč). Co uděláte?"
        msg_choice = {"Dopřeju si": "dilemma_22_a", "Uskromním se": "dilemma_22_b"}
        send_message(sender_id, msg_pre)

    # added dilemma
    elif command == "dilemma_22_a":
        if get_value(sender_id,"difficulty") == "easy": penalty = -4000
        elif get_value(sender_id,"difficulty") == "medium": penalty = -3500
        elif get_value(sender_id,"difficulty") == "hard": penalty = -3000
        msg_text = "Po nákupu tedy máte " + str(set_score(sender_id, penalty)) +" Kč. Při jeho ukládání ale zjistíte, že vaše stará lednička definitivně dosloužila. Co teď? Investujete dalších 4500 Kč do nové?"
        msg_choice = {"Ledničku potřebuju": "dilemma_3_a", "Poradím si jinak": "dilemma_3_b"}
    elif command == "dilemma_22_b":
        if get_value(sender_id,"difficulty") == "easy": penalty = -2000
        elif get_value(sender_id,"difficulty") == "medium": penalty = -1800
        elif get_value(sender_id,"difficulty") == "hard": penalty = -1500        
        msg_text = "Po nákupu tedy máte " + str(set_score(sender_id, penalty)) +" Kč. Při jeho ukládání ale zjistíte, že vaše stará lednička definitivně dosloužila. Co teď? Investujete dalších 4500 Kč do nové?"
        msg_choice = {"Ledničku potřebuju": "dilemma_3_a", "Poradím si jinak": "dilemma_3_b"}

    # third dilemma
    elif command == "dilemma_3_a":
        msg_pre = "Tahle koupě sice vaši peněženku pořádně zabolí, ale nová lednička má ekonomičtější provoz než ta předchozí, takže se z dlouhodobého hlediska vyplatí. (V reportáži nahlédněte do nákupního košíku běžné důchodkyně: http://rozhl.as/fg1)"
        msg_text = "Ještě neuplynulo ani čtrnáct dní a váš účet ukazuje " + str(set_score(sender_id, -4500)) + " korun. Je zřejmé, že tenhle měsíc bude hodně náročný. Přátelé vám doporučí krátkodobou brigádu, přivydělávají si přece i lidi ve vyšším věku. Rozhodnete se pro tohle mimořádné řešení?"
        msg_choice = {"Zkusím to": "dilemma_4_a", "Raději ne": "dilemma_4_b"}
        send_message(sender_id, msg_pre)
    elif command == "dilemma_3_b":
        msg_pre = "Ušetřili jste sice zatím za novou ledničku, ale na balkóně se vám zkazila část nakoupených potravin, takže musíte hned udělat další nákup za 1500 Kč. (V reportáži nahlédněte do nákupního košíku běžné důchodkyně: http://rozhl.as/fg1)"
        msg_text = "Ještě neuplynulo ani čtrnáct dní a váš účet ukazuje " + str(set_score(sender_id,-1500)) + " korun. Je zřejmé, že tenhle měsíc bude hodně náročný. Přátelé vám doporučí krátkodobou brigádu, přivydělávají si přece i lidi ve vyšším věku. Rozhodnete se pro tohle mimořádné řešení?"
        msg_choice = {"Zkusím to": "dilemma_4_a", "Raději ne": "dilemma_4_b"}
        send_message(sender_id, msg_pre)

    # fourth dilemma
    elif command == "dilemma_4_a":
        if get_value(sender_id,"difficulty") == "medium":
            msg_pre = "Po týdnu brigádničení při třídění pošty jste svůj rozpočet vylepšili o 3000 Kč. Dlouhé hodiny ohýbání vám ale daly tak zabrat, že se skoro nemůžete hýbat. Musíte navštívit lékaře, který vám předepíše léky proti bolesti a hojivou mast. Za doplatky v lékárně necháte 400 korun."
        else:
            msg_pre = "Po týdnu brigádničení jako ostraha v supermarketu jste svůj rozpočet vylepšili o 3000 Kč. Dlouhé hodiny stání vám ale daly tak zabrat, že se skoro nemůžete hýbat. Musíte navštívit lékaře, který vám předepíše léky proti bolesti a hojivou mast. Za doplatky v lékárně necháte 400 korun."
        msg_text = "Jste na " + str(set_score(sender_id, 2600)) + " Kč a musíte vystačit ještě víc než týden. Přiblížil se termín výletu klubu seniorů do Českého Krumlova. Váháte, jestli na výlet za tisíc korun jet. Přátelé vás přemlouvají, dlouho jste se na výlet těšili. Dopřejete si společnost a zábavu?"
        msg_choice = {"Chci si užít": "dilemma_5_a", "Je to nad mé poměry": "dilemma_5_b"}
        send_message(sender_id, msg_pre)
    elif command == "dilemma_4_b":
        msg_pre = "Možná pro vás brigáda není to pravé, možná vám ji nedovolí zdraví, možná máte jiné důvody. Koždopádně si bez mimořádného příjmu budete muset odepřít například některé volnočasové aktivity nebo nákup něčeho, co by vám udělalo radost, možná se omezit i při nákupu potravin."
        msg_text = "Jste na " + str(get_value(sender_id,"score")) + " Kč a musíte vystačit ještě víc než týden. Přiblížil se termín výletu klubu seniorů do Českého Krumlova. Váháte, jestli na výlet za tisíc korun jet. Přátelé vás přemlouvají, dlouho jste se na výlet těšili. Dopřejete si společnost a zábavu?"
        msg_choice = {"Chci si užít": "dilemma_5_a", "Je to nad mé poměry": "dilemma_5_b"}
        send_message(sender_id, msg_pre)

    # fifth dilemma
    elif command == "dilemma_5_a":
        msg_pre = "Výlet se opravdu povedl, podobné drobné radosti jsou k nezaplacení."
        msg_text = "Měsíc končí a vaše bilance je " + str(set_score(sender_id, -1000)) + " Kč. Čeká vás ale opravdu špatná zpráva. Majitel bytu, ve kterém léta bydlíte, vám oznámil, že od nového roku zvyšuje nájemné o 2000 korun. To je na vás skutečně moc. Horečně řešíte, co dělat."
        msg_choice = {"Zůstanu, kde jsem": "dilemma_6_a", "Najdu levnější byt": "dilemma_6_b"}
        send_message(sender_id, msg_pre)
    elif command == "dilemma_5_b":
        msg_pre = "Ačkoli je vám to velmi nepříjemné, musíte výlet zrušit. Před přáteli se vymluvíte na bolavá kolena a víkend prosedíte doma. Nudíte se, je vám trochu smutno, ale výlet, který jste dlouho plánovali, si prostě tentokrát nemůžete dovolit."
        msg_text = "Měsíc končí a vaše bilance je " + str(get_value(sender_id,"score")) + " Kč. Čeká vás ale opravdu špatná zpráva. Majitel bytu, ve kterém léta bydlíte, vám oznámil, že od nového roku zvyšuje nájemné o 2000 korun. To je na vás skutečně moc. Horečně řešíte, co dělat."
        msg_choice = {"Zůstanu, kde jsem": "dilemma_6_a", "Najdu levnější byt": "dilemma_6_b"}
        send_message(sender_id, msg_pre)

    # finishing
    elif command == "dilemma_6_a":
        final_score = get_value(sender_id, "score")
        msg_pre_1 = "Kde ale vezmete peníze na vyšší nájemné?"
        msg_pre_2 = "Mohli byste znovu shánět přivýdělek, hledat společné bydlení, vypůjčit si peníze u dětí, banky či nebankovní instituce. Každá z těchto variant má své výhody i nevýhody a není vždy dostupná každému. (V reportáži zjistíte, jak bytovou situaci řeší čeští důchodci: http://rozhl.as/fg3)"
        msg_pre_3 = "Nebudeme vás už dál trápit, umíte si už asi představit, jak náročné je žít z důchodu, který v České republice v průměru činí 12 678 Kč pro muže a 10 416 Kč pro ženy."
        msg_skrblik = "Racionálními rozhodnutími se Vám podařilo uspořit poměrně dost peněz. V blízké budoucnosti byste se ale i tak mohli snadno ocitnout ve složité finanční situaci. Důchodci v reálném životě musí řešit například náklady na léky, ošacení a obuv, dopravu, drogerii atd., které jsme do hry nezahrnuli."
        msg_minus = "Se svými financemi jste skončili v mínusu. Museli byste si vypůjčit a hrozilo by vám spadnutí do dluhové pasti. Taková situace je pro skutečné seniory neúnosná."
        msg_text = "Díky, že jste s námi hráli! Hra vznikla v rámci týdne Českého rozhlasu Tahle země bude pro starý. Sledujte vysílání i sociální sítě Radiožurnálu a nezapomeňte navštívit stránku http://jeziskovavnoucata.cz. Hru teď můžete zkusit znova a rozhodovat se jinak."
        msg_choice = {"Chci vědět víc": "info", "Nová hra": "start"}
        send_message(sender_id, msg_pre_1)
        wait(sender_id)
        send_message(sender_id, msg_pre_2)   
        if final_score >= 2000:
            wait(sender_id)
            send_message(sender_id, msg_skrblik)     
        elif final_score < 0:
            wait(sender_id)
            send_message(sender_id, msg_minus)                    
        wait(sender_id)
        send_message(sender_id, msg_pre_3)
    elif command == "dilemma_6_b":
        final_score = get_value(sender_id, "score")
        msg_pre_1 = "Podařilo se vám najít slušný podnájem za stejné peníze, za jaké jste bydleli doteď, ale majitel po vás chce okamžitě zaplatit kauci ve výši dvou měsíčních nájmů, jinak byt pronajme jinému zájemci. Co teď?"
        msg_pre_2 = "Mohli byste znovu shánět přivýdělek, hledat společné bydlení, vypůjčit si peníze u dětí, banky či nebankovní instituce. Každá z těchto variant má své výhody i nevýhody a není vždy dostupná každému. (V reportáži zjistíte, jak bytovou situaci řeší čeští důchodci: http://rozhl.as/fg3)"
        msg_pre_3 = "Nebudeme vás už dál trápit, umíte si už asi představit, jak náročné je žít z důchodu, který v České republice v průměru činí 12 678 Kč pro muže a 10 416 Kč pro ženy."
        msg_skrblik = "Racionálními rozhodnutími se Vám podařilo uspořit poměrně dost peněz. V blízké budoucnosti byste se ale i tak mohli snadno ocitnout ve složité finanční situaci. Důchodci v reálném životě musí řešit například náklady na léky, ošacení a obuv, dopravu, drogerii atd., které jsme do hry nezahrnuli."
        msg_minus = "Se svými financemi jste skončili v mínusu. Museli byste si vypůjčit a hrozilo by vám spadnutí do dluhové pasti. Taková situace je pro skutečné seniory neúnosná."
        msg_text = "Díky, že jste s námi hráli! Hra vznikla v rámci týdne Českého rozhlasu Tahle země bude pro starý. Články k tématu najdete na https://www.irozhlas.cz/zpravy-tag/zeme-senioru. Zjistěte o projektu víc nebo si hru zahrajte znovu."
        msg_choice = {"Chci vědět víc": "info", "Nová hra": "start"}
        send_message(sender_id, msg_pre_1)
        wait(sender_id)
        send_message(sender_id, msg_pre_2)   
        if final_score >= 2000:
            wait(sender_id)
            send_message(sender_id, msg_skrblik)          
        elif final_score < 0:
            wait(sender_id)
            send_message(sender_id, msg_minus)      
        wait(sender_id)
        send_message(sender_id, msg_pre_3)
    elif command == "info":
        msg_pre_1 = "Tematický týden Českého rozhlasu Tahle země bude pro starý se od 27. listopadu do 3. prosince věnuje problematice stáří a stárnutí populace."
        msg_pre_2 = "Tématem se zabývá mimo jiné Radiožurnál. Každý den v 8:50 přiblíží redaktor Ľubomír Smatana finanční situaci seniorů. Do týdne se zapojí i Český rozhlas Dvojka nebo Plus."
        msg_text = "Tematický týden je součástí projektu Ježíškova vnoučata, ve kterém lidé plní přání babiček a dědečků, kteří svou aktuální životní etapu prožívají v domovech pro seniory. Přidejte se k Ježíškovým vnoučatům na adrese https://www.jeziskovavnoucata.cz/!"
        msg_choice = {"Nová hra": "start"}
        send_message(sender_id, msg_pre_1)        
        wait(sender_id)
        send_message(sender_id, msg_pre_2)

    # first time negative message
    try:
        if get_value(sender_id, "score") < 0 and get_value(sender_id, "wasnegative") == 0:
            msg_negative = "Pozor, právě jste se s měsíčním rozpočtem dostali do mínusu. Musíte tedy sáhnout do úspor. Snad to je jen dočasná situace, kterou se vám podaří brzo zlepšit."
            send_message(sender_id, msg_negative)        
            set_wasnegative(sender_id)
    except:
        pass

    msg = choice(msg_text, msg_choice)
    wait(sender_id)
    send(msg, sender_id)

# choice generator  
def choice(text, buttons):
    message = json.dumps({
            "attachment":{
                "type":"template",
                "payload":{
                "template_type":"button",
                "text":text,
                "buttons":[button(entry, buttons[entry]) for entry in buttons]
                }
            }
        })

    return message

# button generator
def button(title, command):
    button = json.dumps({
        "type":"postback",
        "title":title,
        "payload":command
        })
    return button

# fallback for message events
def process_message(sender_id, message_text):
    if message_text == "start" or message_text == "restart" or message_text == "Zajímá mě to":
        game(sender_id, "start")
    elif message_text == "see me":
        mark_seen(sender_id)
    elif message_text == "type":
        typing_on(sender_id)
    elif message_text == "stop typing":
        typing_off(sender_id)
    else:
        send_message(sender_id, 'Vašemu požadavku jsme nerozuměli. Ve hře klikejte na zobrazené možnosti. Pokud chcete hru spusit znovu, napište "restart".')

# simple message sending
def send_message(recipient_id, message_text):
    message = json.dumps({
            "text": message_text
        })
    send(message, recipient_id)

# general message sending API
def send(message, recipient_id):
    params = {
        "access_token": os.environ["PAGE_ACCESS_TOKEN"]
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "messaging_type": "RESPONSE",
        "recipient": {
            "id": recipient_id
        },
        "message": message
        })      
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)

# sender action sending
def sender_action(recipient_id, action):
    params = {
        "access_token": os.environ["PAGE_ACCESS_TOKEN"]
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": recipient_id
        },
        "sender_action": action
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)

# functions for calling sender actions
def mark_seen(recipient_id):
    sender_action(recipient_id, "mark_seen")

def typing_on(recipient_id):
    sender_action(recipient_id, "typing_on")

def typing_off(recipient_id):
    sender_action(recipient_id, "typing_off")

def wait(sender_id,time=3):
    typing_on(sender_id)
    sleep(time)
    typing_off(sender_id)

# database actions
def get_value(sender_id, column):
    table = boto3.resource('dynamodb').Table('chatbot_duchod_score')
    response = table.query(KeyConditionExpression=Key('fbid').eq(sender_id))
    value = response["Items"][0][column]
    return value 

def init_score(sender_id, initscore, difficulty):
    table = boto3.resource('dynamodb').Table('chatbot_duchod_score')
    insert = table.put_item(Item={'fbid': sender_id, 'score': initscore, 'difficulty': difficulty, 'wasnegative': 0})
    return initscore

def set_score(sender_id, scorechange):
    table = boto3.resource('dynamodb').Table('chatbot_duchod_score')
    score = get_value(sender_id,"score")
    insert = table.update_item(Key={'fbid': sender_id}, UpdateExpression="set score = :sc", ExpressionAttributeValues={':sc': score + scorechange})
    return score + scorechange

def set_wasnegative(sender_id):
    table = boto3.resource('dynamodb').Table('chatbot_duchod_score')
    score = get_value(sender_id,"score")
    insert = table.update_item(Key={'fbid': sender_id}, UpdateExpression="set wasnegative = :neg", ExpressionAttributeValues={':neg': 1})
    return score + scorechange
